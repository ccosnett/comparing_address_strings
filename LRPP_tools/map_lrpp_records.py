from .lrpp_mapping_helper_fns import *
#from MISC_tools import dict_handling_tools as dht
import dask.dataframe as dd
import json
import csv
from datetime import date



def map_lrpp_records(dataset_location):
    
    # load the dataset into a dask dataframe
    print("Loading Land Registry Price Paid Dataset ...")
    cols_to_use = [0,2,3,7,8,9,10,11] # dataset has no headers
    lrpp = dd.read_csv(dataset_location, usecols = cols_to_use, header=None, keep_default_na = False, dtype=str)
    lrpp.columns = ["TIN", "Date of Transfer", "Postcode", "PAON", "SAON", "Street", "Locality", "Town/City"]

    # create address line fields consisting of strings of the full address
    lrpp["Addressline"] = lrpp["PAON"] + ", " + lrpp["SAON"] + ", " + lrpp["Street"] + ", " + lrpp["Town/City"] + ", " + lrpp["Postcode"]
    lrpp["Addressline2"] = lrpp["SAON"] + ", " + lrpp["PAON"] + ", " + lrpp["Street"] + ", " + lrpp["Town/City"] + ", " + lrpp["Postcode"]
    
    print("Completed")

    results = lrpp.map_partitions(map_lower_function, meta=int).to_frame().compute()
    
    print('results saved ')
    results.columns = ["results"]
    matched_records = [i for i in results["results"] if i != None]
    print('nones  ')
    with open('lrpp_match_' + str(date.today()) + '.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        for dic in matched_records:
            for k, v in dic.items():
                array = [k, dic[k][0]]
                writer.writerow(array)

                #    merged_matched_records = dht.merge_dicts(matched_records)
                #   print('merged ')


                # sorted_merged_matched_records = dht.sort_dict_elements(merged_matched_records)
                #  print('sorted ')


                #  dht.write_to_csv('matched_lrpp_records_',merged_matched_records)
                # print('dht.writ ')

    return 1


if __name__ == "__main__":
    main()