import re
import pandas as pd
import numpy as np
from pandarallel import pandarallel
from multiprocessing.pool import ThreadPool
import dask
dask.config.set(pool=ThreadPool(1))
pandarallel.initialize(nb_workers=12)
import sqlite3

import os
home = os.path.expanduser("~")

def check_match(addh_record, lrpp_record):
    address1 = addh_record["address1line"].replace(",", "").replace(" ", "")
    address21 = lrpp_record["Addressline"].replace(",", "").replace(" ", "")
    address22 = lrpp_record["Addressline2"].replace(",", "").replace(" ", "")
    
    if address1 == address21:
        return address1
    elif address1 == address22:
        return address1
    else:
        # check if addh_record["Building Name"] is a number address composite and split the field if it is
        if re.match("[0-9]+[a-z]*\,*\s(.*)", addh_record["buildingname"]):
            match = re.match("([0-9]+[a-z]*)\,*\s(.*)", addh_record["buildingname"]).groups()
            b_num = match[0]
            b_name = match[1]

            # catch cases where the street field name has been incorrectly filled with the building name
            if lrpp_record["PAON"].isdigit() and lrpp_record["SAON"] == "":
                if lrpp_record["PAON"] == b_num and lrpp_record["Street"] == b_name:
                    return addh_record["address1line"]



            # check for match with PAON and SAON
            if lrpp_record["PAON"] != "" and lrpp_record["SAON"] != "":
                if lrpp_record["PAON"] == b_name and lrpp_record["SAON"] == b_num:
                    return addh_record["address1line"]

        else:

            # check for PAON and SAON filled
            if lrpp_record["PAON"] != "" and lrpp_record["SAON"] != "":

                # check for matches of PAON and SAON in building name and building number respectively
                if lrpp_record["PAON"] == addh_record["buildingname"] and (lrpp_record["SAON"] == addh_record["buildingnumber"] or lrpp_record["SAON"] == addh_record["subbuildingname"]):
                    return addh_record["address1line"]

                # or the other way around, including possibly a match to the sub building name field
                elif lrpp_record["PAON"] == addh_record["buildingnumber"] and (lrpp_record["SAON"] == addh_record["buildingname"] or lrpp_record["SAON"] == addh_record["subbuildingname"]):
                    return addh_record["address1line"]


            # if PAON is filled and SAON is empty
            elif lrpp_record["PAON"] != "" and lrpp_record["SAON"] == "":

                # check for a PAON and Street name fields of the LRPP record with the building number and thoroughfare fields in addh
                if lrpp_record["PAON"] == addh_record["buildingnumber"] and lrpp_record["Street"] == addh_record["buildingname"]:
                    return addh_record["address1line"]

                # check for matches of PAON with the building name and number 
                elif lrpp_record["PAON"] == addh_record["buildingname"] or lrpp_record["PAON"] == addh_record["buildingnumber"]:

                    if lrpp_record["Town/City"] == addh_record["posttown"] and (lrpp_record["Street"]==addh_record["thoroughfare"] or lrpp_record["Street"]==addh_record["dependentthoroughfare"]):
                        return addh_record["address1line"]


                # check for matches of PAON with the sub building name or organisation name fields of the addresshub dataset
                elif lrpp_record["PAON"] == addh_record["subbuildingname"] or lrpp_record["PAON"] == addh_record["organisationname"]:
                    if lrpp_record["Street"] != "":
                        if lrpp_record["Street"] == addh_record["thoroughfare"] or lrpp_record["Street"] == addh_record["dependentthoroughfare"]:
                            return addh_record["address1line"]

                    elif lrpp_record["Locality"] != "":
                        if (lrpp_record["Locality"] == addh_record["dependentlocality"] or lrpp_record["Locality"] == addh_record["doubledependentlocality"]):
                            return addh_record["address1line"]


            # or if PAON is empty and SAON is filled
            elif lrpp_record["PAON"] == "" and lrpp_record["SAON"] != "":

                # check for matches of SAON and Street fields in sub building name and thoroughfare fields respectively
                if lrpp_record["SAON"] == addh_record["subbuildingname"] and lrpp_record["Street"] == addh_record["thoroughfare"]:
                    return addh_record["address1line"]

                elif lrpp_record["SAON"] == addh_record["buildingname"] or lrpp_record["SAON"] == addh_record["buildingnumber"]:
                    return addh_record["address1line"]

        # to catch addresses of the format "street/cottage, num"
        if re.match("[a-z]+\s*[a-z]*\,\s[0-9]+[a-z]*", lrpp_record["PAON"]):
            match = re.match("([a-z]+\s*[a-z]*)\,\s([0-9]+[a-z]*)", lrpp_record["PAON"])
            name, num = match.group(1), match.group(2)

            if lrpp_record["SAON"] != "":
                if name == addh_record["buildingname"] or num == addh_record["buildingnumber"]:
                    if lrpp_record["SAON"] == addh_record["subbuildingname"] or lrpp_record["SAON"] == addh_record["thoroughfare"]:
                        return addh_record["address1line"]

            else:
                if name == addh_record["buildingname"] or num == addh_record["buildingnumber"]:
                    return addh_record["address1line"]

        return None


def get_houses_for_postcode(postcode_name):
    conn = sqlite3.connect(home + "/.lqqd_address_mapper/AddressHub/db/addresshub.sqlite")
    q = "SELECT organisationname,buildingnumber, buildingname, subbuildingname,dependentlocality,doubledependentlocality,thoroughfare,dependentthoroughfare,posttown,address1line FROM addresshub WHERE postcode=\'{}\'".format(postcode_name)
    return pd.read_sql_query(q, conn)



def test_lrpp_record(lrpp_record):
    sub_paf = get_houses_for_postcode(lrpp_record["Postcode"])

    try:
        sub_paf = get_houses_for_postcode(lrpp_record["Postcode"])
    except:
        return None
    res = sub_paf.apply(check_match, lrpp_record=lrpp_record, axis=1)
    res = np.vstack(list(res))
    
    match_locations = (res[:,0] != None)
    num_matches = np.sum(match_locations)

    
    if num_matches == 1:
        addressline_of_match = sub_paf.iloc[np.where(res[:,0] != None)[0]]["address1line"].values[0]
        return {addressline_of_match: [lrpp_record["TIN"], lrpp_record["Date of Transfer"]]}
    else:
        return None


    
def map_lower_function(lrpp_records):
    a = lrpp_records.parallel_apply(test_lrpp_record,axis=1)
    return a