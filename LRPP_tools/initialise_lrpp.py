import os
from MISC_tools import download_file
import fileinput
from .map_lrpp_records import map_lrpp_records

def initialise_lrpp():
    # create relevant directories

    home = os.path.expanduser("~")
    os.makedirs(home + "/.lqqd_address_mapper/LRPP/New")
    os.makedirs(home + "/.lqqd_address_mapper/LRPP/Old")

    
    # current working directory
    curr_wd = home + "/.lqqd_address_mapper/LRPP/Old/"
    
    # download most recent lrpp dataset from www.gov.uk/government/statistical-data-sets/price-paid-data-downloads"
    
    print("Downloading most recent file from www.gov.uk/government/statistical-data-sets/price-paid-data-downloads")
    url_of_download = "http://prod.publicdata.landregistry.gov.uk.s3-website-eu-west-1.amazonaws.com/pp-complete.csv"
    
    #download the file
    download_file.download_file(url_of_download, location = curr_wd)
    
    print("Converting pp-complete.csv to lower case for faster mapping later")
    for line in fileinput.input(curr_wd + "pp-complete.csv", inplace=1):
        print(line.lower(), end='')
    print("Converting Completed")
    
    # performing mapping of records to AddressHub.
    #<--- currently this needs to take place in one go and the results are written to the file at the end. It
    # may be worth exploring a periodic system of saving and tracking where the computation is ---->
    print("Mapping records to AddressHub")
    matched_records = map_lrpp_records(curr_wd + "pp-complete.csv")
    
    print("LRPP Dataset Initialised")
    
    return 1