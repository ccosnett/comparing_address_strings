import os
import requests
import re
import zipfile
from MISC_tools import download_file
import sqlite3
from tqdm import tqdm
import pandas as pd
import fileinput
import time
from dotenv import load_dotenv
import ast

def initialise_addresshub():
    start = time.time()
    # create relevant directories
    home = os.path.expanduser("~")
    os.makedirs(home + "/.lqqd_address_mapper/AddressHub/New")
    os.makedirs(home + "/.lqqd_address_mapper/AddressHub/Old")
    os.makedirs(home + "/.lqqd_address_mapper/AddressHub/db")
    
    # current working directory
    curr_wd = home + "/.lqqd_address_mapper/AddressHub/Old/"
    
    # download most recent addresshub dataset from datavault.europa.uk.com
    # here we use the library "requests" to fetch the most recent dataset from the table
    # listed on datavault.europa.uk.com. As of 28/03/2021 the table is defaulted to sort by date of creation
    # descending from the most recent. Thus the most recent file is the top entry in the table.
    # <----- this could be changed in the future to look at the dates that come with the files to pick
    # the most recent ----->
    
    print("Downloading most recent file from datavault.europa.uk.com")
    # create correct url to download file
    
    with requests.Session() as session:
        load_dotenv()
        payload = ast.literal_eval(os.getenv('AH_payload'))
        post = session.post("https://datavault.europa.uk.com/edv/process.php?do=login", data=payload)
        cookie_dict = session.cookies.get_dict()# cookie file
    
    url_of_page="https://datavault.europa.uk.com/edv/my_files/"

    request_response = requests.post(url_of_page, cookies=cookie_dict)
    html_of_page = request_response.text
  
    matches = re.search(r'download&amp;id=(.*)" target="_blank"><strong>(.*)</strong></a></td>', html_of_page)
    download_code = matches.group(1)
    file_name = matches.group(2) + ".zip"
    url_of_download = "https://datavault.europa.uk.com/edv/process.php?do=download&id=" + download_code
    download_file.download_file(url_of_download, filename=file_name, location = curr_wd, cookie=cookie_dict) #download the file
    name_of_zip_file = [f for f in os.listdir(curr_wd) if f.endswith('zip')][0]
    
    
    
    #unzip the file to the same directory it is located in
    print("Unzipping New File")
    with zipfile.ZipFile(curr_wd + name_of_zip_file, 'r') as zip_ref:
        zip_ref.extractall(curr_wd)    
    print("Unzipping Completed")
    
    
    
    # convert csv file to lowercase now to enable faster address mapping later
    name_of_csv_file = "_".join(name_of_zip_file[:-3].split(" ")) + "csv"
    print("Converting", name_of_csv_file, "to lower case for faster mapping later")
    for line in fileinput.input(curr_wd + name_of_csv_file, inplace=1):
        print(line.lower(), end='')
    print("Converting completed")
    prev_wd = curr_wd
    curr_wd = home + "/.lqqd_address_mapper/AddressHub/db"
    
    
    
    # create sqlite database for later querying
    # populate database with csv file, null fields are stored as strings. We iterate through the csv file
    # in chunks of "chunksize" lines
    print("Building SQLite database")
    db = sqlite3.connect(curr_wd + "/addresshub.sqlite")
    for line in pd.read_csv(prev_wd + name_of_csv_file, keep_default_na=False,chunksize=100000, dtype=str):
        line.to_sql("addresshub", db, if_exists="append")
    print("Building Completed")
    
    # create an index on the postcode field for faster querying later
    db.execute("CREATE INDEX postcode ON addresshub(postcode)")
    db.close()
    
    print("AddressHub Dataset Initialised")
    end = time.time()
    
    print("Total runtime was", (end-start)/60, "minutes")
    
    
    return 1