import numpy as np
import pandas as pd
import re
import dask.dataframe as dd
import sqlite3
from multiprocessing.pool import ThreadPool
import dask
dask.config.set(pool=ThreadPool(1))
from pandarallel import pandarallel
pandarallel.initialize(nb_workers=10)


import os
home = os.path.expanduser("~")

def check_match(addh_record, epc_record):
    
    if epc_record["ADDRESS1"] =="" and epc_record["ADDRESS2"] =="" and epc_record["ADDRESS3"]=="":
        return None
    
    address1 = addh_record["address1line"].replace(",", "").replace(" ", "")
    address2 = epc_record["ADDRESS_LINE"].replace(",", "").replace(" ", "")
    
    if address1 == address2:
        return address1
    else:
        if re.match("[0-9]+[a-z]*\s[a-z]+", addh_record["buildingname"]):
            match = re.match("([0-9]+[a-z]*)\s*(.*)", addh_record["buildingname"]).groups()

            b_num = match[0]
            b_name = match[1]

        
        # some addresshub records have a building name of 0. This is discarded for the purpose of matching
        # and the building number field is treated as empty.
        elif addh_record["buildingnumber"] == "0":
            b_num = ""
            b_name = addh_record["buildingname"]

        else:
            b_num = addh_record["buildingnumber"]
            b_name = addh_record["buildingname"]


  
        # separate the ADDRESS2 field into its number and building name components
        match = re.match("([0-9]*\-*[0-9]*[a-z]*)\,*\s*(.*)", epc_record["ADDRESS2"]).groups()
        address2_group1 = match[0]
        address2_group2 = match[1]



        # check the ADDRESS1 field for evidence of a flat/apartment and if present
        flat = 0 # indicator variable to track success in any of the following checks
        if re.match("flat\s", epc_record["ADDRESS1"]):
            match = re.match("(flat\s[0-9]*\-*[0-9]*[a-z]*)\,*\s*(.*)", epc_record["ADDRESS1"]).groups()
            address1_group1 = match[0]
            address1_group2 = match[1]
            flat = 1

        elif re.match("ug[0-9]+\s", epc_record["ADDRESS1"]):
            match = re.match("(ug[0-9]+)\,*\s*(.*)", epc_record["ADDRESS1"]).groups()
            address1_group1 = match[0]
            address1_group2 = match[1]
            flat = 1

        elif re.match("apartment\s", epc_record["ADDRESS1"]):
            match = re.match("(apartment\s[0-9]*\-*[0-9]*[a-z]*)\,*\s*(.*)", epc_record["ADDRESS1"]).groups()
            address1_group1 = match[0]
            address1_group2 = match[1]
            flat = 1


        # in the case that the ADDRESS1 field references a flat,check the following:
        if flat:
            if re.match("[0-9]+", address2_group1) and address1_group1 == addh_record["subbuildingname"]:

                    if address2_group2 == addh_record["thoroughfare"] and  (address2_group1 == b_num or address2_group1 == b_name):

                        return addh_record["address1line"]

                    elif address1_group2 + " " + address2_group1 == b_name and address2_group2 == addh_record["thoroughfare"]:
                        return addh_record["address1line"]


            elif address1_group1 == addh_record["subbuildingname"] and (epc_record["ADDRESS2"] == b_name or address1_group2 == b_name):
                return addh_record["address1line"]

   
        # If the ADDRESS1 field begins with a number
        elif epc_record["ADDRESS1"] != "": 
            if epc_record["ADDRESS1"][0].isdigit():
                match = re.match("([0-9]+\-*[0-9]*[a-z]*)\,*\s*(.*)", epc_record["ADDRESS1"]).groups()

                address1_group1 = match[0]
                address1_group2 = match[1]

                if b_name == "":
                    if address1_group1 == b_num  and (address1_group2 == addh_record["thoroughfare"] or address1_group2 == addh_record["dependentthoroughfare"]):
                        return addh_record["address1line"]
                else:
                    if address1_group1 == b_num and address1_group2 == b_name:
                        return addh_record["address1line"]

                    elif address1_group1 == b_name and address1_group2 == addh_record["thoroughfare"]:
                        return addh_record["address1line"]


                if epc_record["ADDRESS2"] != "":
                    if address2_group1 == addh_record["buildingnumber"] and address2_group2 == addh_record["thoroughfare"]:

                        if address1_group1 == addh_record["subbuildingname"] and address1_group2 == addh_record["buildingname"]:
                            return addh_record["address1line"]

                    elif (address1_group2 + " " + address2_group1 == addh_record["buildingname"] or 
                          address1_group2 == b_name) and address1_group1 == addh_record["subbuildingname"] and address2_group2 == addh_record["thoroughfare"]:
                        return addh_record["address1line"]

                    elif address1_group2 == b_name and address1_group1 == addh_record["subbuildingname"] and epc_record["ADDRESS2"] == addh_record["thoroughfare"]:
                        return addh_record["address1line"]


        # if none of the previous rules have matched try these rules
        if epc_record["ADDRESS1"] == addh_record["buildingname"]:
            return addh_record["address1line"]

        elif epc_record["ADDRESS1"] == addh_record["subbuildingname"]:
            if address2_group1 == addh_record["buildingnumber"] and address2_group2 == addh_record["thoroughfare"]:
                return addh_record["address1line"]
            elif epc_record["ADDRESS2"] == addh_record["buildingname"]:
                return addh_record["address1line"]

        return None



def get_houses_for_postcode(postcode_name):
    conn = sqlite3.connect(home + "/.lqqd_address_mapper/AddressHub/db/addresshub.sqlite")
    q = "SELECT organisationname,buildingnumber, buildingname, subbuildingname,dependentlocality,doubledependentlocality,thoroughfare,dependentthoroughfare,posttown,address1line FROM addresshub WHERE postcode=\'{}\'".format(postcode_name)
    return pd.read_sql_query(q, conn)


def test_epc_record(epc_record):
    # Load the sub_addh dataset corresponding to the postcode of the input "record"
    # If it's empty return None

    sub_paf = get_houses_for_postcode(epc_record["POSTCODE"])
    
    if sub_paf.empty:
        return None
    
    # check for a match in the sub_addh against the inputted epc record provided to this function
    res = sub_paf.apply(check_match, epc_record=epc_record, axis=1)
    res = np.vstack(list(res))
    
    
    # If there are more than two possible matches it may be a possibility that its due to multiple spellings of
    # the post town name. If so, take the record with the same spelling.
    match_locations = (res[:,0] != None)
    num_matches = np.sum(match_locations)
    
    if num_matches > 1:
        double_matches = np.where(match_locations)[0]

        if epc_record["POSTTOWN"] == sub_paf.iloc[double_matches[0]]["posttown"] and epc_record["POSTTOWN"] != sub_paf.iloc[double_matches[1]]["posttown"]:
            address_of_match = sub_paf.iloc[double_matches[0]]["address1line"]

        elif epc_record["POSTTOWN"] == sub_paf.iloc[double_matches[1]]["posttown"] and epc_record["POSTTOWN"] != sub_paf.iloc[double_matches[0]]["posttown"]:
            address_of_match = sub_paf.iloc[double_matches[1]]["address1line"]

        else:
            del sub_paf
            return None
            
    elif num_matches == 1:
        address_of_match = sub_paf.iloc[np.where(res[:,0] != None)[0]]["address1line"].values[0]
        del sub_paf
        
    else:
        del sub_paf
        return None

    return [address_of_match, epc_record["LMK_KEY"]]

def map_lower_function(epc_records):
    a = epc_records.parallel_apply(test_epc_record,axis=1)
    return a