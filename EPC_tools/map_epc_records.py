from .epc_mapping_helper_fns import *
import dask.dataframe as dd
import csv
from datetime import date

def map_epc_records(dataset_location):
    
    filename = dataset_location.split("/")[-1]
    
    # load the dataset into a dask dataframe
    print("Loading Energy Performance Certificate Dataset ...")
    cols_to_use = ["lmk_key", "address1", "address2", "address3", "postcode","building_reference_number", "county","lodgement_date","posttown"]
    epc = dd.read_csv(dataset_location, header=0,usecols = cols_to_use, dtype=str, keep_default_na=False)
    epc.columns = ["LMK_KEY", "ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTCODE","BUILDING_REFERENCE_NUMBER",  "COUNTY", "LODGEMENT_DATE","POSTTOWN"]
    epc["ADDRESS_LINE"] = epc["ADDRESS1"] + ", " + epc["ADDRESS2"] + ", " + epc["ADDRESS3"] + ", " + epc["POSTTOWN"] + ", " + epc["POSTCODE"]
    print("Completed")


    results = epc.map_partitions(map_lower_function, meta=int).to_frame().compute()
    
    # results consists of a pandas dataframe where each row is a dictionary
    # each dictionary contains a single key-value pair consisting of the following
    #              { "address1line of addresshub record": ["string containing LMK_KEY of matched EPC certificate"] }
    # we want to process these into an output csv file consisting of the columns:
    #                                address1line, lmk_key
    # The following steps achieve this
    
    results.columns = ["results"]
    matched_records = [i for i in results["results"].tolist() if i != None]

    pd.DataFrame(matched_records).to_csv('matched_epc_records_' + str(date.today()) + ".csv", index=False)

                
    return 1

if __name__ == "__main__":
    main()