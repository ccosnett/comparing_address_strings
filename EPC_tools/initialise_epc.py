import os
import zipfile
from MISC_tools import download_file
import shutil
import fileinput
from .map_epc_records import map_epc_records
from dotenv import load_dotenv
import requests


def initialise_epc():
    # create relevant directories
    home = os.path.expanduser("~")
    os.makedirs(home + "/.lqqd_address_mapper/EPC/New")
    os.makedirs(home + "/.lqqd_address_mapper/EPC/Old")
    
    # current working directory
    curr_wd = home + "/.lqqd_address_mapper/EPC/Old/"
    
    # download most recent addresshub dataset from datavault.europa.uk.com
    # here we use the library "requests" to fetch the most recent dataset from the table
    # listed on datavault.europa.uk.com. As of 28/03/2021 this is the top most file in the table.
    # <----- this could be changed in the future to look at the dates that come with the files to pick
    # the most recent ----->
    
    print("Downloading most recent file from epc.opendatacommunities.org")
    with requests.Session() as session:
        load_dotenv()
        response = session.get(os.getenv('EPC_login_link'))
        cookie_dict = session.cookies.get_dict()

    # create correct url to download file
    url_of_download="https://epc.opendatacommunities.org/files/all-domestic-certificates.zip"
    
    #download the file
    download_file.download_file(url_of_download, location = curr_wd, cookie=cookie_dict)
    
    name_of_zip_file = [f for f in os.listdir(curr_wd) if f.endswith('.zip')][0]
    
    #unzip the file to the same directory it is located in
    print("Unzipping New File")
    os.mkdir(curr_wd + "unzipped_files")
    with zipfile.ZipFile(curr_wd + name_of_zip_file, 'r') as zip_ref:
        zip_ref.extractall(curr_wd + "unzipped_files")    
    print("Unzipping Completed")
    
    # join epc files together
    print("Joining EPC files together into single csv")
    print("Additionally converting to lowercase for fast future mapping")
    list_of_csvs = list()
    for (dirpath, dirnames, filenames) in os.walk(curr_wd + "unzipped_files"):
        list_of_csvs += [os.path.join(dirpath, file) for file in filenames if file == "certificates.csv"]
    
    with open(curr_wd + 'epc_all.csv', 'w') as outfile:
        for fname in list_of_csvs:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line.lower())
    print("Joining Completed")
    
    # delete unzipped files
    print("Deleting unzipped files")
    shutil.rmtree(curr_wd + "unzipped_files")
    print("Deleting Completed")
    
    
    # convert csv file to lowercase now to enable faster address mapping later
    print("Converting epc_all.csv to lower case for faster mapping later")
    for line in fileinput.input(curr_wd + "epc_all.csv", inplace=1):
        print(line.lower(), end='')
    print("Done")
    
    
    print("Mapping records to AddressHub")
    # map dataset to addresshub and print matches to file
    matched_records = map_epc_records(curr_wd + "epc_all.csv")
    print("EPC Dataset Initialised")
    
    return 1