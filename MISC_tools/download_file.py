import requests 
import tqdm     # progress bar
import os.path
import datetime
from dateutil.parser import parse as parsedate

def download_file(url, location, filename="", verbose = False, cookie={}, previous_file=""):
    """
    Download file with progressbar
    """
    if filename == "":
        local_filename = os.path.join(".",url.split('/')[-1])
    else:
        local_filename = filename
        
    # get content length from response headers
    if cookie:
        r = requests.get(url, cookies=cookie, stream=True)
    else:
        r = requests.get(url, stream=True)
    file_size = int(r.headers.get('Content-Length'))
    
    
    # in the case that only a new file should be downloaded, a check is made
    # using the last modified headers of the response and an older user provided file.
    if previous_file != "":
        url_time = r.headers['last-modified']
        url_date = parsedate(url_time).replace(tzinfo=None)
        file_time = datetime.datetime.fromtimestamp(os.path.getmtime(previous_file)).replace(tzinfo=None)
        os.path.getmtime(previous_file)
        file_time = datetime.datetime.fromtimestamp(os.path.getmtime(previous_file))
        
        if url_date > file_time:
            download_check = 1
        else:
            download_check = 0
    else:
        download_check = 1
    
    # if we should download, we download.
    if download_check:
        chunk = 1
        chunk_size=1024
        num_bars = int(file_size / chunk_size)
        if verbose:
            print(dict(file_size=file_size))
            print(dict(num_bars=num_bars))

        with open(location + "/" + local_filename, 'wb') as fp:
            for chunk in tqdm.tqdm(
                                        r.iter_content(chunk_size=chunk_size)
                                        , total= num_bars
                                        , unit = 'KB'
                                        , desc = local_filename
                                        , leave = True # progressbar stays
                                    ):
                fp.write(chunk)
        print("Download Completed")
    else:
        print("File has not been modified.")
    return download_check